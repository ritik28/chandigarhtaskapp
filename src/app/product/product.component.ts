import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  productForm: any;
  items: any;
  constructor(private formBuilder: FormBuilder) { } // injecting formbuilder here 

  ngOnInit(): void {
    this.productForm = this.formBuilder.group({                 //intializing form here 
      items: this.formBuilder.array([ this.createItem() ])
    });
  }

  //method for create new form controls on click
  createItem(): FormGroup {
    return this.formBuilder.group({
      productName:  ['',Validators.required],
      productDescription: ['',Validators.required],
      productColor:  ['',Validators.required],
      productSize:  ['',Validators.required],
      productWeight:  ['',Validators.required],
      productQuantity: ['',Validators.required],
    });
  }

  addItem(): void {                                               // call addItem method in the template when the user clicks to add a new item.//
    this.items = this.productForm.get('items') as FormArray;
    this.items.push(this.createItem());
  }
}
